#!/bin/sh

set -eu

workdir="$(mktemp -d)"
#trap "rm -rf ${workdir}" INT TERM EXIT
mkdir -p repo
sources=$(readlink -f sources)
repo=$(readlink -f repo)
cd "${workdir}"

download_deb() {
  local base wdir
  base="${1}"
  wdir="deb-$(echo "${base}" | sed -e 's/https:\/\///; s/\//-/g')"
  mkdir "${wdir}"
  cd "${wdir}"
  wget "${base}/InRelease"
  gpg --verify InRelease
  wget "${base}/Packages"
  sed -e '/^SHA256:/,/^\S/ !d' InRelease  | awk '{ if ($3 == "Packages") { print($1 " Packages") }}' | sha256sum -c -
  grep-dctrl -n -s SHA256,Filename '' Packages | sed -z 's/\n/ /g; s/\.deb/.deb\n/g;' | sed 's/^\s*//' > hashes
  while read hash package; do
    wget --continue "${base}/${package}"
  done < hashes
  sha256sum -c hashes
  cp *.deb "${repo}/"
  cd - > /dev/null
}

download_rpm() {
  local base primary
  base="${1}"

  wdir="rpm-$(echo "${base}" | sed -e 's/https:\/\///; s/\//-/g')"
  mkdir "${wdir}"
  cd "${wdir}"

  wget --quiet "${base}"/repodata/repomd.xml
  wget --quiet "${base}"/repodata/repomd.xml.asc
  gpg --verify repomd.xml.asc

  primary=$(xpath -n -q -e "/repomd/data[@type='primary']/location/@href" repomd.xml | sed -e 's/\s*href="\(.*\)"/\1/')
  wget "${base}/${primary}"
  primary_checksum=$(xpath -q -n -e '/repomd/data[@type="primary"]/checksum[@type="sha256"]/text()' repomd.xml)
  primary_gz="$(basename "${primary}")"
  gunzip --keep "${primary_gz}"
  wget "${base}/${primary}"
  echo "${primary_checksum} ${primary_gz}" > hashes
  sha256sum -c hashes

  primary_xml="$(basename "${primary_gz}" .gz)"
  pkg=1
  while true; do
    pkgxml=pkg-${pkg}.xml
    xpath -q -e "/metadata/package[${pkg}]" "${primary_xml}" > "${pkgxml}"
    if test -s "${pkgxml}"; then
      h=$(xpath -q -e '/package/checksum[@type="sha256"]/text()' "${pkgxml}")
      rpm=$(xpath -q -e 'string(/package/location/@href)' "${pkgxml}")
      f=$(basename "${rpm}")
      wget "${base}/${rpm}"
      echo "${h} ${f}" | sha256sum -c -
      cp ${f} "${repo}/"
    else
      break
    fi
    pkg=$((pkg+1))
  done

  cd - > /dev/null
}

for src in $(ls -1 ${sources}/*.source); do
  url=$(cat "${src}")
  key=${src%.source}.asc
  gpg --import "${key}"
  download_deb "${url}"
  download_rpm "${url}"
done
